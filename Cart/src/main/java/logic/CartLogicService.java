package logic;

import api.api.ICartLogicService;
import api.dto.CartDTO;
import api.dto.CartPageDTO;
import persistence.CartPersistence;
import persistence.api.ICartPersistence;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
@Default
@Stateless
@LocalBean
public class CartLogicService implements ICartLogicService {

    @Inject
    protected CartPersistence persistance;

    public CartDTO createCart(CartDTO cart){
        return persistance.createCart( cart);
    }

    public List<CartDTO> getCarts(){
        return persistance.getCarts();
    }

    public CartPageDTO getCarts(Integer page, Integer maxRecords){
        return persistance.getCarts(page, maxRecords);
    }

    public CartDTO getCart(Long id){
        return persistance.getCart(id);
    }

    public void deleteCart(Long id){
        persistance.deleteCart(id);
    }

    public void updateCart(CartDTO cart){
        persistance.updateCart(cart);
    }
}
