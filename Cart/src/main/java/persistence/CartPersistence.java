package persistence;

import api.dto.CartDTO;
import api.dto.CartPageDTO;
import persistence.api.ICartPersistence;
import persistence.converter.CartConverter;
import persistence.entity.CartEntity;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
@Default
@Stateless
@LocalBean
public class CartPersistence implements ICartPersistence {
    @PersistenceContext(unitName="MarketplaceProjectPU")

    protected EntityManager entityManager;

    public CartDTO createCart(CartDTO cart) {
        CartEntity entity= CartConverter.persistenceDTO2Entity(cart);
        entityManager.persist(entity);
        return CartConverter.entity2PersistenceDTO(entity);
    }

    @SuppressWarnings("unchecked")
    public List<CartDTO> getCarts() {
        Query q = entityManager.createQuery("select u from CartEntity u");
        return CartConverter.entity2PersistenceDTOList(q.getResultList());
    }

    @SuppressWarnings("unchecked")
    public CartPageDTO getCarts(Integer page, Integer maxRecords) {
        Query count = entityManager.createQuery("select count(u) from CartEntity u");
        Long regCount = 0L;
        regCount = Long.parseLong(count.getSingleResult().toString());
        Query q = entityManager.createQuery("select u from CartEntity u");
        if (page != null && maxRecords != null) {
            q.setFirstResult((page-1)*maxRecords);
            q.setMaxResults(maxRecords);
        }
        CartPageDTO response = new CartPageDTO();
        response.setTotalRecords(regCount);
        response.setRecords(CartConverter.entity2PersistenceDTOList(q.getResultList()));
        return response;
    }

    public CartDTO getCart(Long id) {
        return CartConverter.entity2PersistenceDTO(entityManager.find(CartEntity.class, id));
    }

    public void deleteCart(Long id) {
        CartEntity entity=entityManager.find(CartEntity.class, id);
        entityManager.remove(entity);
    }

    public void updateCart(CartDTO detail) {
        CartEntity entity=entityManager.merge(CartConverter.persistenceDTO2Entity(detail));
        CartConverter.entity2PersistenceDTO(entity);
    }
}
