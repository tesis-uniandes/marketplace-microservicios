package service;

import api.api.IItemLogicService;
import api.dto.ItemDTO;
import api.dto.ItemPageDTO;
import logic.ItemLogicService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Mauricio on 3/23/15.
 */
@Path("/Item")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ItemService {
    @Inject
    protected ItemLogicService itemLogicService;

    @POST
    public ItemDTO createItem(ItemDTO item){
        return itemLogicService.createItem(item);
    }

    @DELETE
    @Path("{id}")
    public void deleteItem(@PathParam("id") Long id){
        itemLogicService.deleteItem(id);
    }

    @GET
    public ItemPageDTO getItems(@QueryParam("page") Integer page, @QueryParam("maxRecords") Integer maxRecords){
        return itemLogicService.getItems(page, maxRecords);
    }

    @GET
    @Path("{id}")
    public ItemDTO getItem(@PathParam("id") Long id){
        return itemLogicService.getItem(id);
    }

    @PUT
    public void updateItem(@PathParam("id") Long id, ItemDTO item){
        itemLogicService.updateItem(item);
    }

}
