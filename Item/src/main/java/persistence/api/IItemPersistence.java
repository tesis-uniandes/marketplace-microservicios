package persistence.api;

import api.dto.ItemDTO;
import api.dto.ItemPageDTO;

import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
public interface IItemPersistence {
    public ItemDTO createItem(ItemDTO detail);
    public List<ItemDTO> getItems();
    public ItemPageDTO getItems(Integer page, Integer maxRecords);
    public ItemDTO getItem(Long id);
    public void deleteItem(Long id);
    public void updateItem(ItemDTO detail);
}
