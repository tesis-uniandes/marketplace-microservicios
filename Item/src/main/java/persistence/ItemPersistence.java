package persistence;

import api.dto.ItemDTO;
import api.dto.ItemPageDTO;
import persistence.api.IItemPersistence;
import persistence.converter.ItemConverter;
import persistence.entity.ItemEntity;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
//@Default
@Stateless
@LocalBean
public class ItemPersistence implements IItemPersistence{
    @PersistenceContext(unitName="MarketplaceProjectPU")

    protected EntityManager entityManager;

    public ItemDTO createItem(ItemDTO item) {
        ItemEntity entity=ItemConverter.persistenceDTO2Entity(item);
        entityManager.persist(entity);
        return ItemConverter.entity2PersistenceDTO(entity);
    }

    @SuppressWarnings("unchecked")
    public List<ItemDTO> getItems() {
        Query q = entityManager.createQuery("select u from ItemEntity u");
        return ItemConverter.entity2PersistenceDTOList(q.getResultList());
    }

    @SuppressWarnings("unchecked")
    public ItemPageDTO getItems(Integer page, Integer maxRecords) {
        Query count = entityManager.createQuery("select count(u) from ItemEntity u");
        Long regCount = 0L;
        regCount = Long.parseLong(count.getSingleResult().toString());
        Query q = entityManager.createQuery("select u from ItemEntity u");
        if (page != null && maxRecords != null) {
            q.setFirstResult((page-1)*maxRecords);
            q.setMaxResults(maxRecords);
        }
        ItemPageDTO response = new ItemPageDTO();
        response.setTotalRecords(regCount);
        response.setRecords(ItemConverter.entity2PersistenceDTOList(q.getResultList()));
        return response;
    }

    public ItemDTO getItem(Long id) {
        return ItemConverter.entity2PersistenceDTO(entityManager.find(ItemEntity.class, id));
    }

    public void deleteItem(Long id) {
        ItemEntity entity=entityManager.find(ItemEntity.class, id);
        entityManager.remove(entity);
    }

    public void updateItem(ItemDTO detail) {
        ItemEntity entity=entityManager.merge(ItemConverter.persistenceDTO2Entity(detail));
        ItemConverter.entity2PersistenceDTO(entity);
    }

}
