package persistence;

import api.dto.ProductDTO;
import api.dto.ProductPageDTO;
import persistence.api.IProductPersistence;
import persistence.converter.ProductConverter;
import persistence.entity.ProductEntity;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
//@Default
@Stateless
@LocalBean
public class ProductPersistence implements IProductPersistence{

    @PersistenceContext(unitName = "MarketplaceProjectPU")
    protected EntityManager entityManager;

    public ProductDTO createProduct(ProductDTO product) {
        ProductEntity entity= ProductConverter.persistenceDTO2Entity(product);
        entityManager.persist(entity);
        return ProductConverter.entity2PersistenceDTO(entity);
    }

    @SuppressWarnings("unchecked")
    public List<ProductDTO> getProducts() {
        Query q = entityManager.createQuery("select u from ProductEntity u");
        return ProductConverter.entity2PersistenceDTOList(q.getResultList());
    }

    @SuppressWarnings("unchecked")
    public ProductPageDTO getProducts(Integer page, Integer maxRecords) {
        Query count = entityManager.createQuery("select count(u) from ProductEntity u");
        Long regCount = 0L;
        regCount = Long.parseLong(count.getSingleResult().toString());
        Query q = entityManager.createQuery("select u from ProductEntity u");
        if (page != null && maxRecords != null) {
            q.setFirstResult((page-1)*maxRecords);
            q.setMaxResults(maxRecords);
        }
        ProductPageDTO response = new ProductPageDTO();
        response.setTotalRecords(regCount);
        response.setRecords(ProductConverter.entity2PersistenceDTOList(q.getResultList()));
        return response;
    }

    public ProductDTO getProduct(Long id) {
        return ProductConverter.entity2PersistenceDTO(entityManager.find(ProductEntity.class, id));
    }

    public void deleteProduct(Long id) {
        ProductEntity entity=entityManager.find(ProductEntity.class, id);
        entityManager.remove(entity);
    }

    public void updateProduct(ProductDTO detail) {
        ProductEntity entity=entityManager.merge(ProductConverter.persistenceDTO2Entity(detail));
        ProductConverter.entity2PersistenceDTO(entity);
    }

    public String hola(){
        Query q = entityManager.createQuery("select u from ProductEntity u");
        return ProductConverter.entity2PersistenceDTOList(q.getResultList()).toString();
//        return entityManager.isOpen()+"";
    }
}
