package example;

import persistence.converter.ProductConverter;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by Mauricio on 3/23/15.
 */
@Stateless
@LocalBean
public class ProductLogicService {

    @PersistenceContext
    private EntityManager em;

    @PersistenceContext(unitName = "MarketplaceProjectPU")
    protected EntityManager entityManager;

    public String getProducts(){
        Query q = em.createQuery("select u from ProductEntity u");
        return ProductConverter.entity2PersistenceDTOList(q.getResultList()).toString();
//        return "injection de dependenca"+em.isOpen()+"OM:"+entityManager.isOpen();
    }
}
